

    Streams     |    length   |   k     |   d   |  drift speed*   | noise level   |
    ------------|-------------|---------|-------|-----------------|---------------|
1   K Change 1  |    50.000   |  10+-2  |  10   |  100            | 0             |
2   K Change 2  |    50.000   |  10+-2  |  20   |  100            | 0             |
3   K Change 3  |    50.000   |  10+-2  |  5    |  100            | 0             |
4   K Change 4  |    50.000   |  20+-4  |  10   |  100            | 0             |
5   K Change 5  |    50.000   |  4+-1   |  10   |  100            | 0             |


*: (Kernels move a predefined distance of 0.01 every speed points.) 
k : cluster count
d : dimensions



    Streams     |    length   |   k     |   d   |  drift speed*   | noise level   | Radius  |
    ------------|-------------|---------|-------|-----------------|---------------|---------|
1   K Change 1  |    50.000   |  10+-2  |  10   |  100            | 0             |	0.025   |
2   K Change 2  |    50.000   |  10+-2  |  20   |  100            | 0             | 0.025   |
3   K Change 3  |    50.000   |  10+-2  |  5    |  100            | 0             | 0.025   |
4   K Change 4  |    50.000   |  20+-4  |  10   |  100            | 0             | 0.025   |
5   K Change 5  |    50.000   |  4+-1   |  10   |  100            | 0             | 0.025   |